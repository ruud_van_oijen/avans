package domain;

import domain.Item;
import java.util.Comparator;

/**
 * Created by ruudoijen on 06-07-15.
 *
 * @author ruudoijen
 */
public class ItemPreparationTimeComparator implements Comparator<Item> {


    @Override
    public int compare(Item item1, Item item2) {
        Integer item1Prep = item1.getRealpreperationTime();
        Integer item2Prep = item2.getRealpreperationTime();

        if (item1Prep == item2Prep)
            return 0;
        else if (item1Prep > item2Prep)
            return 1;
        else
            return -1;
    }
}
