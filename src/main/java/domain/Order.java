/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import datastorage.ItemDAO;

import java.util.*;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class Order {

	private int orderId;
	private String sendOn;
	private String lastUpdate;
	private int statusId;
	private int employee;
	private ArrayList<Item> itemArrayList;
    private boolean orderPlan;

	public Order(int orderId, String sendOn, String lastUpdate, int status, int employee) {
		ItemDAO items = new ItemDAO();
		itemArrayList = new ArrayList<Item>();
        itemArrayList = items.findOrderItems(orderId);

        this.orderPlan = false;
		this.orderId = orderId;
		this.sendOn = sendOn;
		this.lastUpdate = lastUpdate;
		this.statusId = status;
		this.employee = employee;
	}

    public boolean getOrderPlan() {
        return orderPlan;
    }

    public void setOrderPlan(boolean orderPlan) {
        this.orderPlan = orderPlan;
    }

    public void setItemArrayList(ArrayList<Item> itemArrayList) { this.itemArrayList = itemArrayList; }

    public ArrayList<Item> getItemArrayList() {
		return this.itemArrayList;
	}

	public int getOrderId() {
		return this.orderId;
	}

	public String getSendOn() {
		return this.sendOn;
	}

	public String getLastUpdate() {
		return this.lastUpdate;
	}

	public int getStatusId() {
		return this.statusId;
	}

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

	public int getEmployee() {
		return this.employee;
	}


	@Override
	public boolean equals(Object o) {
		boolean equal = false;

		if (o == this) {
			// Dezelfde instantie van de klasse, dus per definitie hetzelfde.
			equal = true;
		} else {
			if (o instanceof Order) {
				Order l = (Order) o;

				// Boek wordt geidentificeerd door ISBN, dus alleen hierop
				// controlleren is voldoend.
				equal = this.orderId == l.orderId;
			}
		}

		return equal;
	}

	@Override
	public int hashCode() {
		// Deze implementatie is gebaseerd op de best practice zoals beschreven
		// in Effective Java, 2nd edition, Joshua Bloch.

		// membershipNumber is uniek, dus voldoende als hashcode.
		return orderId;
	}
}
