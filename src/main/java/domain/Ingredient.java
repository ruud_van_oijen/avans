package domain;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class Ingredient {

    private int ingredientId;
    private String ingredientName;
    private int amount;

    public Ingredient(int ingredientId, String ingredientName, int amount) {
        this.ingredientId = ingredientId;
        this.ingredientName = ingredientName;
        this.amount = amount;
    }

    public int getIngredientId() {
        return ingredientId;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public int getAmount() {
        return amount;
    }
}
