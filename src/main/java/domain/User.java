package domain;

import java.util.ArrayList;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public abstract class User {

	protected int employeeId;
    protected int employeeCode;
    protected String employeeFirstName;
    protected String employeeLastName;
    protected String addres;
    protected String zipcode;
    protected String city;
    protected String email;
    protected String dateOfBirth;
    protected int employeePosition;
    protected ArrayList<Item> items;

    public User(){
    }

	public User(int employeeId, int employeeCode, String employeeFirstName, String employeeLastName, String addres, String zipcode, String city,
                String email, String dateOfBirth, int employeePosition) {
        this.items = new ArrayList<Item>();
		this.employeeId = employeeId;
		this.employeeCode = employeeCode;
		this.employeeFirstName = employeeFirstName;
		this.employeeLastName = employeeLastName;
		this.addres = addres;
		this.zipcode = zipcode;
		this.city = city;
		this.email = email;
		this.dateOfBirth = dateOfBirth;
		this.employeePosition = employeePosition;

	}

    public ArrayList<Item> getItems() {
        return items;
    }
    public abstract void addToItems(Item item);

    public int getEmployeeId() {
        return employeeId;
    }

    public int getEmployeeCode() {
        return employeeCode;
    }

    public String getEmployeeFirstName() {
        return employeeFirstName;
    }

    public String getEmployeeLastName() {
        return employeeLastName;
    }

    public String getAddres() { return addres; }

    public String getZipcode() {
        return zipcode;
    }

    public String getCity() {
        return city;
    }

    public String getEmail() {
        return email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public int getEmployeePosition() {
        return employeePosition;
    }

    public void removeItemfromUser(Item item1){
        for(Item item2 : items){
            if(item1.equals(item2)){
                items.remove(item1);
            }
        }
    }

    /**
     *
     * abstract method
     * @param object abstract method
     * @return abstract method
     */
	public abstract boolean equals(Object object);

    /**
     * abstract method
     * @return abstract method
     */
	public abstract int hashCode();
}
