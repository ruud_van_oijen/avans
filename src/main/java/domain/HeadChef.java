package domain;

import java.util.ArrayList;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class HeadChef extends User {

    public HeadChef(int employeeId, int employeeCode, String employeeFirstName, String employeeLastName, String addres, String zipcode, String city,
                    String email, String dateOfBirth, int employeePosition) {
        super();
        this.items = new ArrayList<Item>();
        this.employeeId = employeeId;
        this.employeeCode = employeeCode;
        this.employeeFirstName = employeeFirstName;
        this.employeeLastName = employeeLastName;
        this.addres = addres;
        this.zipcode = zipcode;
        this.city = city;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.employeePosition = employeePosition;
    }

    public void addToItems(Item item){
        items.add(item);
    }

    /**
     * @param object object
     * @return true if object is the same as the current object
     * override abstract function of the class user
     * Check whether the object is the same is as the current object
     */
    public boolean equals(Object object) {
        if (object instanceof HeadChef && ((HeadChef) object).getEmployeeId() == this.employeeId) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return employee id
     * override abstract function employeenumber
     */
    public int hashCode() {
        return employeeId;
    }
}
