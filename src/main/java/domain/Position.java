package domain;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class Position {

    private int employeePositionId;
    private String employeePositionName;

    public Position(int employeePositionId, String employeePositionName) {
        this.employeePositionId = employeePositionId;
        this.employeePositionName = employeePositionName;
    }

    public int getPositionId() {
        return employeePositionId;
    }

    public String getPositionName() {
        return employeePositionName;
    }

}
