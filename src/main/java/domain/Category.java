package domain;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class Category {
    private int categoryId;
    private String categoryName;

    public Category(int categoryId, String categoryName){
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }
    public int getCategoryId() {
        return categoryId;
    }
    public String getCategoryName() {
        return categoryName;
    }
}
