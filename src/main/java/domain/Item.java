package domain;


import datastorage.IngredientDAO;

import java.util.ArrayList;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class Item implements Comparable<Item> {

	private int itemId;
	private String itemName;
	private int employeeId;
	private String employeeName;
	private String orderStatus;
    private ArrayList<Ingredient> ingredientArrayList;

	private int statusId;
	private int preparationTime;
	private int categoryId;
	private int amount;
    private String description;

	public Item(int itemId, String itemName, int employeeId, String employeeName, String orderStatus, int amount, int preparationTime, int categoryId, String description) {
		IngredientDAO ingredients = new IngredientDAO();
		ingredientArrayList = new ArrayList<Ingredient>();
		ingredientArrayList = ingredients.findItemIngredients(itemId);
        this.statusId = 1;
		this.itemId = itemId;
		this.itemName = itemName;
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.orderStatus = orderStatus;
		this.amount = amount;
        this.preparationTime = preparationTime;
        this.categoryId = categoryId;
        this.description = description;
	}


    public int getPreparationTime() { return preparationTime; }

    public int getCategoryId() { return categoryId; }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getStatusId() {
		return statusId;
	}

	public int getItemId() {
		return itemId;
	}

    public String getDescription() {
        return description;
    }



    public ArrayList<Ingredient> getIngredientArrayList() {
        return ingredientArrayList;
    }

    public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getRealpreperationTime(){
		int prep = preparationTime;
		prep = prep * amount;
		return prep;
	}
	/**
	 * @param object object
	 * @return true if object is the same as the current object
	 * override abstract function of the class Item
	 * Check whether the object is the same is as the current object
	 */
	public boolean equals(Object object) {
		if (object instanceof Item && ((Item) object).getItemId() == this.itemId) {
			return true;
		} else {
			return false;
		}
	}


    /**
    *  Implement the natural order for this class
    */
    public int compareTo(Item item2)
    {
        Integer itemNmbr = this.getRealpreperationTime();
        Integer item2Nmbr = item2.getRealpreperationTime();

        return itemNmbr.compareTo(item2Nmbr);
    }

}
