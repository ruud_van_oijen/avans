package domain;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class Status {
    private int statusId;
    private String statusName;

    public Status(int statusId, String statusName){
        this.statusId = statusId;
        this.statusName = statusName;
    }
    public int getStatusId() {
        return statusId;
    }
    public String getStatusName() {
        return statusName;
    }
}
