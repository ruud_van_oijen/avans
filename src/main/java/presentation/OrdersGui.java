package presentation;

import businesslogic.*;
import domain.User;
import domain.Order;
import domain.Position;
import domain.Status;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class OrdersGui extends JPanel {

	private HapManager hapManager;

	private DefaultTableModel ordersDTM;
	private DefaultTableModel employeesDTM = new DefaultTableModel(0, 0);

	private JTextField barcodeField;
	private JButton barcodebutton;
    private HashMap<Integer, Order> hmOrders;


	private JTable table;
    private JTable loggedInTable;

    private HashMap<Integer, Status> statusList;
    private HashMap<Integer, Position> positionList;
    private OrderGui orderView;
    private ProfileGui profileView;
    private DishGui dishView;

    private JButton button2;

	public OrdersGui(HapManager hapManager) {
        statusList = hapManager.getStatusList();
        positionList = hapManager.getPositionList();
		this.ordersDTM = new DefaultTableModel(0, 0);
		this.hapManager = hapManager;

		// this JFrame
		this.setLayout(new BorderLayout());

		JPanel orderCont = new JPanel();
		CardLayout cl = new CardLayout();
		orderCont.setLayout(cl);

        //create the dish panel
        JPanel dishPanel = createDishView();
        //create the profile panel
        JPanel profilePanel = createProfileView();
		//create the order panel
		JPanel orderPanel = createOrderView();
		// create the Order Table
		JPanel tablePanel = createTablePanel();
		// create the loggedInPanel
		JPanel loggedInPanel = createloggedInPanel();

        hapManager.setOrderCl(cl);
        hapManager.setOrderContainer(orderCont);
        hapManager.setOrderView(orderView);
        hapManager.setProfileView(profileView);
        hapManager.setDishView(dishView);


		orderCont.add(tablePanel, "ordersoverview");
		orderCont.add(orderPanel, "itemsoverview");
        orderCont.add(profilePanel, "profile");
        orderCont.add(dishPanel, "dish");

		orderCont.setSize(1024, 768);

		cl.show(orderCont, "orderoverzicht");

		this.add(orderCont, BorderLayout.CENTER);
		this.add(loggedInPanel, BorderLayout.EAST);

		final HapManager member = hapManager;

		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
                try{
                    ordersDTM.setRowCount(0);
                    member.findOrders();
                    doFindOrders();
                }catch(NoActiveOrdersException naoe){
                    JOptionPane.showMessageDialog(new JFrame(), naoe.toString());
                }
			}
		});

        table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    JTable target = (JTable) e.getSource();

                    try{
                        int row = target.getSelectedRow();
                        Object value = target.getValueAt(row, 0);
                        int orderId = Integer.parseInt(value.toString());
                        member.changeOrder(orderId);
                        member.setOrderviewtwee();

                    }catch(NullPointerException npe){
                        JOptionPane.showMessageDialog(new JFrame(),
                                npe.toString());
                    }catch(NumberFormatException nfe){
                        JOptionPane.showMessageDialog(new JFrame(),
                                nfe.toString());
                    }catch(ArrayIndexOutOfBoundsException aiobe){
                        JOptionPane.showMessageDialog(new JFrame(),
                                aiobe.toString());
                    }catch(InvalidOrderNumber ion){
                        JOptionPane.showMessageDialog(new JFrame(),
                                ion.toString());
                    }

                }
            }
        });

        loggedInTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    JTable target = (JTable) e.getSource();

                    try{
                        int row = target.getSelectedRow();
                        //int column = target.getSelectedColumn();
                        // colum is altijd 0 want daar staat het profile_Barcode
                        Object value = target.getValueAt(row, 0);
                        // check op profile_Barcode
                        int profileId = Integer.parseInt(value.toString());
                        member.setProfile(profileId);
                        member.setProfileView();


                    }catch(NullPointerException npe){
                        JOptionPane.showMessageDialog(new JFrame(),
                                npe.toString());
                    }catch(NumberFormatException nfe){
                        JOptionPane.showMessageDialog(new JFrame(),
                                nfe.toString());
                    }catch(ArrayIndexOutOfBoundsException aiobe){
                        JOptionPane.showMessageDialog(new JFrame(),
                                new String("Klik op een medewerker"));
                    }

                }
            }
        });

        class BarcodeLogin implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                String inputString = barcodeField.getText();

                // try and catch
                try{
                    int employeeNumber = Integer.parseInt(inputString);
                    member.secondfindEmployee(employeeNumber);
                    barcodeField.setText("");

                }catch(NoActiveUsersException naue){
                    JOptionPane.showMessageDialog(new JFrame(),
                            naue.toString());
                    barcodeField.setText("");
                    barcodeField.requestFocusInWindow();
                }catch(InvalidLogoutException iloe ){
                    JOptionPane.showMessageDialog(new JFrame(),
                            iloe.toString());
                    barcodeField.setText("");
                    barcodeField.requestFocusInWindow();
                }catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Dit is geen geldig nummer");
                    barcodeField.setText("");
                    barcodeField.requestFocusInWindow();
                }
                catch(InvalidEmployeeException nfe){
                    JOptionPane.showMessageDialog(new JFrame(),
                            nfe.toString() );
                    barcodeField.setText("");
                    barcodeField.requestFocusInWindow();
                }catch(NullPointerException nfe){
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Dit is geen geldig nummer");
                    barcodeField.setText("");
                    barcodeField.requestFocusInWindow();

                }catch(HeadChefException cke){
                    JOptionPane.showMessageDialog(new JFrame(),
                            cke.toString());
                    barcodeField.setText("");
                    barcodeField.requestFocusInWindow();
                }
            }
        }

		barcodebutton.addActionListener(new BarcodeLogin());
		barcodeField.addActionListener(new BarcodeLogin());
	}

    /**
     *
     * @return JPanel with orders view
     */
	private JPanel createOrderView() {
		JPanel secondwrap = new JPanel(new GridLayout(1, 1));
		OrderGui second = new OrderGui(hapManager);
        this.orderView = second;
		secondwrap.add(second);
		return secondwrap;
	}

    /**
     *
     * @return JPanel with profile view
     */
    private JPanel createProfileView() {
        JPanel secondwrap = new JPanel(new GridLayout(1, 1));
        ProfileGui profile = new ProfileGui(hapManager);
        this.profileView = profile;
        secondwrap.add(profile);
        return secondwrap;
    }

    /**
     *
     * @return JPanel with item view
     */
    private JPanel createDishView() {
        JPanel secondwrap = new JPanel(new GridLayout(1, 1));
        DishGui dish = new DishGui(hapManager);
        this.dishView = dish;
        secondwrap.add(dish);
        return secondwrap;
    }

    /**
     *
     * @return JPanel with Orders view
     */
	private JPanel createTablePanel() {
        JPanel orderswrapper = new JPanel();
        button2 = new JButton();
        JPanel tablePanel = new JPanel(new GridLayout(1, 1));
        table = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String header[] = new String[] { "ID", "Datum/Tijd", "Update", "Status" };
        ordersDTM.setColumnIdentifiers(header);
        table.setModel(ordersDTM);
        tablePanel.add(new JScrollPane(table));

        //======== this ========
        orderswrapper.setLayout(new GridBagLayout());
        ((GridBagLayout)orderswrapper.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout)orderswrapper.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)orderswrapper.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 1.0, 1.0E-4};
        ((GridBagLayout)orderswrapper.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

        button2.setText("Refresh");
        orderswrapper.add(button2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

        //======== panel1 ========
        {
            tablePanel.setLayout(new GridLayout(1, 1));
        }
        orderswrapper.add(tablePanel, new GridBagConstraints(0, 1, 4, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));

		return orderswrapper;
	}

    /**
     *
     * @return JPanel with table of active employee's
     */
	private JPanel createloggedInPanel() {

		JPanel loggedInPanel = new JPanel();
		loggedInPanel.setLayout(new BorderLayout(0, 0));

		JLabel label1 = new JLabel("Aanwezig personeel", JLabel.CENTER);
		label1.setVerticalTextPosition(JLabel.BOTTOM);
		label1.setHorizontalTextPosition(JLabel.CENTER);

		loggedInTable = new JTable() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		String header[] = new String[] { "ID", "Naam", "Functie" };

		employeesDTM.setColumnIdentifiers(header);

        loggedInTable.setModel(employeesDTM);

        loggedInTable.setOpaque(false);

		JPanel barcode_wrap = new JPanel();
		barcode_wrap.setLayout(new BoxLayout(barcode_wrap, 2));
		barcodeField = new JTextField(20);
		barcodebutton = new JButton("Login/Logout");
		barcode_wrap.add(barcodeField);
		barcode_wrap.add(barcodebutton);


		loggedInPanel.add(label1, BorderLayout.NORTH);
		loggedInPanel.add(loggedInTable, BorderLayout.CENTER);
		loggedInPanel.add(barcode_wrap, BorderLayout.SOUTH);

		return loggedInPanel;
	}

    /**
     * find all active employees
     */
	public void doFindActiveEmployees() {
		employeesDTM.setRowCount(0);
        HashMap<Integer, User> hmEmployee = new HashMap<>();
        hmEmployee = hapManager.getHmEmployees();

        User chefkok;
        chefkok = hapManager.getChefkok();


		if (chefkok == null && hmEmployee.size() == 0) {
            hapManager.showFirstGui();
            JOptionPane.showMessageDialog(new JFrame(), "Geen actieve gebruikers");
		} else {
            for(int key: hmEmployee.keySet()){
                User user = hmEmployee.get(key);
                int positionId = user.getEmployeePosition();
                Position pos = positionList.get(positionId);
                    employeesDTM.addRow(new Object[] {
                            user.getEmployeeCode(),
                            user.getEmployeeFirstName(),
                            pos.getPositionName()
                    });

            }
		}

	}

    /**
     * find all  orders
     * @throws NoActiveOrdersException when there are no orders
     */
	public void doFindOrders() throws NoActiveOrdersException{
        ordersDTM.setRowCount(0);
        hmOrders = hapManager.getHmOrders();

		if (hmOrders.size() > 0) {
			for (int key : hmOrders.keySet()) {
                int statusId = hmOrders.get(key).getStatusId();
                Status status = statusList.get(statusId);
				ordersDTM.addRow(new Object[] { hmOrders.get(key).getOrderId(),
                        hmOrders.get(key).getSendOn(),
                        hmOrders.get(key).getLastUpdate(),
						status.getStatusName() });
			}

		} else {
            throw new NoActiveOrdersException();
		}
	}

    public void redrawOrders(Order order){
        try{
            order.setStatusId(2);
            hapManager.changeOrderStatus(order);
            doFindOrders();
        }catch(ChangeStatusException cse){
            JOptionPane.showMessageDialog(new JFrame(),
                    cse.toString() );
        }catch (NoActiveOrdersException naoe){
            JOptionPane.showMessageDialog(new JFrame(),
                    naoe.toString() );
        }
    }
}
