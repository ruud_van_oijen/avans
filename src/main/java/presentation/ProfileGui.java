/*
 * Created by JFormDesigner on Thu May 28 12:26:28 CEST 2015
 */

package presentation;

import businesslogic.HapManager;
import domain.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class ProfileGui extends JPanel {

    private HapManager hapManager;
    private HashMap<Integer, Position> positionList;
    private DefaultTableModel ordersDTM;
    private DefaultTableModel timeDTM;

    public ProfileGui(HapManager hapManager) {
        this.hapManager = hapManager;
        this.ordersDTM = new DefaultTableModel(0, 0);
        this.timeDTM = new DefaultTableModel(0, 0);
        this.positionList = hapManager.getPositionList();

        initComponents();

        final HapManager member = hapManager;

        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                member.setOrdervieween();
            }
        });
    }

    /**
     * set the profile to the new user credentials
     *
     *
     * @param user of the current profile information
     */
    public void drawProfile(User user){
        fillItemTable(user); //items appointed to this user (table)
        fillTimeTable(); //Hours worked by this user (table)
        //Surname
        textField1.setText(user.getEmployeeFirstName());
        //name
        textField2.setText(user.getEmployeeLastName());
        //address
        textField3.setText(user.getAddres());
        //City
        textField4.setText(user.getCity());
        //Zip Code
        textField5.setText(user.getZipcode());
        //E Mail
        textField6.setText(user.getEmail());
        //Date Of Birth
        textField7.setText(user.getDateOfBirth());
        Position position = positionList.get(user.getEmployeePosition());
        //Position
        textField9.setText(position.getPositionName());
        //EmployeeCode
        textField8.setText(user.getEmployeeCode()+"");
        textField1.setEditable(false);
        textField2.setEditable(false);
        textField3.setEditable(false);
        textField4.setEditable(false);
        textField5.setEditable(false);
        textField6.setEditable(false);
        textField7.setEditable(false);
        textField8.setEditable(false);
        textField9.setEditable(false);
        revalidate();
        repaint();
    }

    /**
     * initiliasation of the components
     */
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        button1 = new JButton();
        tabbedPane1 = new JTabbedPane();
        panel3 = new JPanel();
        scrollPane2 = new JScrollPane();
        table2 = new JTable();
        panel1 = new JPanel();
        label3 = new JLabel();
        textField1 = new JTextField();
        label4 = new JLabel();
        textField2 = new JTextField();
        label5 = new JLabel();
        textField3 = new JTextField();
        label6 = new JLabel();
        textField4 = new JTextField();
        label7 = new JLabel();
        textField5 = new JTextField();
        label8 = new JLabel();
        textField6 = new JTextField();
        label9 = new JLabel();
        textField7 = new JTextField();
        label10 = new JLabel();
        textField9 = new JTextField();
        label11 = new JLabel();
        textField8 = new JTextField();
        panel2 = new JPanel();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();

        //======== this ========
        setLayout(new GridBagLayout());
        ((GridBagLayout)getLayout()).columnWidths = new int[] {0, 0, 0, 0};
        ((GridBagLayout)getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 1.0E-4};
        ((GridBagLayout)getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

        //---- button1 ----
        button1.setText("Terug");
        add(button1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //======== tabbedPane1 ========
        {

            //======== panel3 ========
            {
                panel3.setLayout(new GridBagLayout());
                ((GridBagLayout)panel3.getLayout()).columnWidths = new int[] {0, 0};
                ((GridBagLayout)panel3.getLayout()).rowHeights = new int[] {0, 0};
                ((GridBagLayout)panel3.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
                ((GridBagLayout)panel3.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

                //======== scrollPane2 ========
                {
                    scrollPane2.setViewportView(table2);
                }
                panel3.add(scrollPane2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            tabbedPane1.addTab("Toegewezen", panel3);

            //======== panel1 ========
            {
                panel1.setLayout(new GridBagLayout());
                ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {71, 0, 198, 0};
                ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 1.0E-4};
                ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

                //---- label3 ----
                label3.setText("Surname:");
                panel1.add(label3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(textField1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label4 ----
                label4.setText("Name:");
                panel1.add(label4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(textField2, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label5 ----
                label5.setText("Address:");
                panel1.add(label5, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(textField3, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label6 ----
                label6.setText("City:");
                panel1.add(label6, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(textField4, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label7 ----
                label7.setText("Zip code:");
                panel1.add(label7, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(textField5, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label8 ----
                label8.setText("E-mail:");
                panel1.add(label8, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(textField6, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label9 ----
                label9.setText("Date of birth:");
                panel1.add(label9, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(textField7, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label10 ----
                label10.setText("Position");
                panel1.add(label10, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(textField9, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label11 ----
                label11.setText("Barcode:");
                panel1.add(label11, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(textField8, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));
            }
            tabbedPane1.addTab("Profiel", panel1);

            //======== panel2 ========
            {
                panel2.setLayout(new GridBagLayout());
                ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {0, 0};
                ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {0, 0};
                ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
                ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(table1);
                }
                panel2.add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            tabbedPane1.addTab("Tijden", panel2);
        }
        add(tabbedPane1, new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
         modifyComponents();
    }

    /**
     * modify components after initilisation
     */
    public void modifyComponents(){
        table2 = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        // add header of the table
        String header[] = new String[] { "ID", "Naam", "Omschrijving", "Status", "Aantal" };
        // add header in table model
        ordersDTM.setColumnIdentifiers(header);
        // set model into the table object
        table2.setModel(ordersDTM);
        scrollPane2.setViewportView(table2);


        table1 = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        // add header of the table
        String header2[] = new String[] { "Datum", "Totaal uren" };
        // add header in table model
        timeDTM.setColumnIdentifiers(header2);
        // set model into the table object
        table1.setModel(timeDTM);
        scrollPane1.setViewportView(table1);

    }

    public void fillTimeTable(){
        timeDTM.setRowCount(0);

        for(int i = 1; i < 31; i++) {
            Random rand = new Random();
            int  n = rand.nextInt(7) + 6;

            timeDTM.addRow(new Object[] {
                    i +" juni 2015",n
            });
        }

    }


    /**
     *
     * @param user object
     * Fill the table with all the items that are appointed to this user
     */
    public void fillItemTable(User user){
        //First, create the lists
        ArrayList<Item> itemArrayList = user.getItems();
        HashMap<Integer, ItemStatus> itemStatusList = hapManager.getItemStatusList();

        //loop through the items and add to the table
        for(int i = 0; i < itemArrayList.size(); i++){
            String statusName = "";

            //compare each status id with the status id from the statuslist
            //start with 1 since first statusid = 1
              for(int j = 1; j < itemStatusList.size(); j++){
                    if(itemStatusList.get(j).getStatusId() == itemArrayList.get(i).getStatusId()){
                        statusName = itemStatusList.get(j).getStatusName();
                    }
                }

            //System.out.println(itemArrayList.get(i).getItemName());
            ordersDTM.addRow(new Object[] {
                    itemArrayList.get(i).getItemId(),
                    itemArrayList.get(i).getItemName(),
                    itemArrayList.get(i).getDescription(),
                    statusName,
                    itemArrayList.get(i).getAmount()
            });
        }
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JButton button1;
    private JTabbedPane tabbedPane1;
    private JPanel panel3;
    private JScrollPane scrollPane2;
    private JTable table2;
    private JPanel panel1;
    private JLabel label3;
    private JTextField textField1;
    private JLabel label4;
    private JTextField textField2;
    private JLabel label5;
    private JTextField textField3;
    private JLabel label6;
    private JTextField textField4;
    private JLabel label7;
    private JTextField textField5;
    private JLabel label8;
    private JTextField textField6;
    private JLabel label9;
    private JTextField textField7;
    private JLabel label10;
    private JTextField textField9;
    private JLabel label11;
    private JTextField textField8;
    private JPanel panel2;
    private JScrollPane scrollPane1;
    private JTable table1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
