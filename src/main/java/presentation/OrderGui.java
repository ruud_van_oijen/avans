/*
 * Created by JFormDesigner on Mon May 25 18:05:32 CEST 2015
 */

package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.*;

import businesslogic.*;
import domain.*;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class OrderGui extends JPanel {


    private HashMap<Integer, ItemStatus> itemStatusHmList = new HashMap<Integer, ItemStatus>();
    private HashMap<Integer, Status> statusHmList = new HashMap<Integer, Status>();
    private HashMap<Integer, User> employeeHmList = new HashMap<Integer, User>();
    private HapManager hapManager;
	private Vector itemStatusComboboxItems;
    private Vector statusComboboxItems;
	private Vector employeeComboboxItems;

    private DefaultComboBoxModel itemStatusModel;
	private DefaultComboBoxModel statusModel;
	private DefaultComboBoxModel employeeModel;
    private int statusFlag = 0;

    private Order order;

	public OrderGui(HapManager hapManager) {

		this.hapManager = hapManager;
        this.list = new ArrayList<Item>();

		statusComboboxItems = new Vector();
		employeeComboboxItems = new Vector();
        itemStatusComboboxItems = new Vector();

		drawStatus();
        drawItemStatus();
		initComponents();


		drawActiveEmployees();

        final HapManager member = hapManager;

        //order plannen
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
                    String input = label2.getText();
                    Integer orderId = Integer.parseInt(input);

                    String inputField2 = JOptionPane.showInputDialog(null, "Chefkok geef u Code:", "Authorisatie Chefkok",
                            JOptionPane.WARNING_MESSAGE);

                    int code = Integer.parseInt(inputField2);

                    User user = employeeHmList.get(code);
                    if(user != null && user instanceof HeadChef ){
                        member.planOrder(orderId);
                    }else{
                        throw new ChefException(user);
                    }
				}catch(NullPointerException npe){
                    JOptionPane.showMessageDialog(new JFrame(),
                            npe.toString());
				}catch(PlannedOrderException poe){
                    JOptionPane.showMessageDialog(new JFrame(),
                            poe.toString());
                }catch(ChefException cfe){
                    JOptionPane.showMessageDialog(new JFrame(),
                            cfe.toString());
                }catch(OrderPlannedException ope){
                    JOptionPane.showMessageDialog(new JFrame(),
                            ope.toString());
                }catch(NumberFormatException nof){
                    JOptionPane.showMessageDialog(new JFrame(),
                            "U heeft het verkeerde barcode ingevoerd.");
                }
			}
		});

        button3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                member.setOrdervieween();
            }
        });
	}

    /**
     *
     * @param order set the current order of the gui
     */
    public void setOrder(Order order){
        this.order = order;
    }

    public JComboBox getComboBox3() {
        return comboBox3;
    }

    /**
     *  draw the order status and put them in the combobox
     */
	public void drawStatus(){
		this.statusHmList = hapManager.getStatusList();
		for(int key: statusHmList.keySet()){
			statusComboboxItems.add(statusHmList.get(key).getStatusName());
		}
		statusModel = new DefaultComboBoxModel(statusComboboxItems);
	}
    /**
     *  draw the item status and put them in the combobox
     */
    public void drawItemStatus(){
        this.itemStatusHmList = hapManager.getItemStatusList();
        for(int key: itemStatusHmList.keySet() ){
            itemStatusComboboxItems.add(itemStatusHmList.get(key).getStatusName());
        }
        itemStatusModel = new DefaultComboBoxModel(itemStatusComboboxItems);
    }
    /**
     * draw current employees and put them in a combobox
     */
	public void drawActiveEmployees(){
		this.employeeHmList = hapManager.getHmEmployees();
        employeeComboboxItems.clear();
        employeeComboboxItems.add("        ");
		for(int key: employeeHmList.keySet() ){
			employeeComboboxItems.add(employeeHmList.get(key).getEmployeeFirstName()+ " " + employeeHmList.get(key).getEmployeeLastName());
		}
		employeeModel = new DefaultComboBoxModel(employeeComboboxItems);
	}

    /**
     * get the index of the combobox that equals the same name as status
     *
     * @param statusId status id
     * @return index of combobox
     */

    public int getIndexStatusForCombobox(int statusId){
        int result = 0;
        for(int i = 0; i < statusComboboxItems.size(); i++){
            String item = statusComboboxItems.get(i).toString();
            for(int key : statusHmList.keySet()){
                Status status = statusHmList.get(key);
                String statusName = status.getStatusName();

                if(status.getStatusId() == statusId && item.equals(statusName)){
                    result = i;
                }
            }
        }
        return result;
    }

    /**
     * get the index of the combobox that equals the same name as status
     *
     * @param statusId status id
     * @return index of combobox
     */

    public int getIndexItemStatusForCombobox(int statusId){
        int result = 0;
        for(int i = 0; i < itemStatusComboboxItems.size(); i++){
            String item = itemStatusComboboxItems.get(i).toString();
            for(int key : itemStatusHmList.keySet()){
                ItemStatus status = itemStatusHmList.get(key);
                String statusName = status.getStatusName();
                if(status.getStatusId() == statusId && item.equals(statusName)){
                    result = i;
                }
            }
        }
        return result;
    }

    /**
     * get the index of the combobox that equals the same name as employee name
     *
     * @param employeeId employee id
     * @return index of combobox
     */

    public int getIndexEmployeeForCombobox(int employeeId){
        int result = 0;
        for(int i = 0; i < employeeComboboxItems.size(); i++){
            String item = employeeComboboxItems.get(i).toString();
            for(int key : employeeHmList.keySet()){
                User user = employeeHmList.get(key);
                String userName = user.getEmployeeFirstName()+ " " + user.getEmployeeLastName();
                if(user.getEmployeeId() == employeeId && item.equals(userName)){
                    result = i;
                }
            }
        }
        return result;
    }

    /**
     *
     * @param statusName of the status you want
     * @return the id of the status in the jcombobox
     */
    public int getStatusIdFromCombobox(String statusName){
        int result = 0;
        for(int key : statusHmList.keySet()){
            Status status = statusHmList.get(key);
            if(status.getStatusName().equals(statusName)){
                result = status.getStatusId();
            }
        }
        return result;
    }

    /**
     *
     * @param statusName of the status you want
     * @return the id of the status in the jcombobox
     */
    public int getItemStatusIdFromCombobox(String statusName){
        int result = 0;
        for(int key : itemStatusHmList.keySet()){
            ItemStatus status = itemStatusHmList.get(key);
            if(status.getStatusName().equals(statusName)){
                result = status.getStatusId();
            }
        }
        return result;
    }

    /**
     *
     * draw the order of the function
     *
     * @param order current order you want to see
     */
	public void drawOrder(Order order){
        statusFlag = 0;
        this.setOrder(order);
        // future timer notification
//        if(order.getOrderPlan()){
//            new TimerGui(hapManager,order);
//        }
        drawActiveEmployees();
		int orderId = order.getOrderId();
		this.label2.setText(orderId+"");
		list = order.getItemArrayList();

        setOrderStatusComboBox();
        statusFlag = 1;

        ArrayList<JPanel> itemsGui = new ArrayList<JPanel>();
        wrap = new JPanel();
        wrap.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;

		scrollPane1.setViewportView(wrap);
		for (Item item : list ) {
            int indexEmployee = getIndexEmployeeForCombobox(item.getEmployeeId());
            int indexItemStatus = getIndexItemStatusForCombobox(item.getStatusId());

			JPanel orderItem = setOrderItems(item, indexItemStatus, indexEmployee, item.getAmount());
			itemsGui.add(orderItem);
		}
		for (JPanel panel : itemsGui) {
			wrap.add(panel, gbc);
		}

        wrap.revalidate();
		wrap.repaint();
	}

    /**
     *
     */
    public void setOrderStatusComboBox() {
        DefaultListSelectionModel model = new DefaultListSelectionModel();
        //model.removeSelectionInterval(3,3);
        int indexCombobox = getIndexStatusForCombobox(order.getStatusId());
        if (indexCombobox >= 2) {
            model.addSelectionInterval(indexCombobox, indexCombobox);
        } else {
            model.addSelectionInterval(indexCombobox, indexCombobox + 1);
        }
        EnabledJComboBoxRenderer enableRenderer = new EnabledJComboBoxRenderer(model);
        comboBox3.setRenderer(enableRenderer);
        comboBox3.setModel(statusModel);
        comboBox3.setSelectedIndex(indexCombobox);
    }

    /**
     *
     * @param item item you want to see
     * @param currentStatus set the status of that item
     * @param currentEmployee set the employee on the item
     * @param amountProduct how many of that item
     * @return JPanel of the item
     */
    public JPanel setOrderItems(Item item, int currentStatus, int currentEmployee, int amountProduct) {
        JPanel orderPanelWrapper = new JPanel();
        JPanel orderPanel = new JPanel();
        JButton itemButton = new JButton("Bekijk "+item.getItemName());
        JTextField hiddenField = new JTextField(""+item.getItemId());
        hiddenField.setVisible(false);
        JLabel employee = new JLabel("Medewerker:");
        JLabel status = new JLabel("status:");
        JLabel amountTitle = new JLabel("Aantal");
        JLabel amount = new JLabel(amountProduct+"");
        Vector statusItems = (Vector)itemStatusComboboxItems.clone();
        DefaultComboBoxModel statusModel = new DefaultComboBoxModel(statusItems);
        JComboBox boxStatus = new JComboBox(statusModel);
        boxStatus.setSelectedIndex(currentStatus);
        boxStatus.setEditable(false);

        if(order.getOrderPlan() == false){
            boxStatus.setEnabled(false);
        }else{
            DefaultListSelectionModel model = new DefaultListSelectionModel();
            int indexCombobox = getIndexItemStatusForCombobox(item.getStatusId());
            if (indexCombobox == 2) {
                model.addSelectionInterval(indexCombobox, indexCombobox);
            } else {
                model.addSelectionInterval(indexCombobox, indexCombobox + 1);
            }
            EnabledJComboBoxRenderer enableRenderer = new EnabledJComboBoxRenderer(model);
            boxStatus.setRenderer(enableRenderer);
            boxStatus.setModel(statusModel);
            boxStatus.setSelectedIndex(indexCombobox);
            boxStatus.setEnabled(true);
        }


        Vector employeeItems = (Vector)employeeComboboxItems.clone();
        DefaultComboBoxModel employeeModel = new DefaultComboBoxModel(employeeItems);
        JComboBox boxEmployee = new JComboBox(employeeModel);
        boxEmployee.setSelectedIndex(currentEmployee);
        boxEmployee.setEditable(false);
        boxEmployee.setEnabled(false);

        orderPanelWrapper.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;

        orderPanel.setLayout(new GridBagLayout());
        ((GridBagLayout)orderPanel.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)orderPanel.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)orderPanel.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)orderPanel.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- button1 ----
        orderPanel.add(itemButton, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

        orderPanel.add(hiddenField, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
        //---- label3 ----
        orderPanel.add(status, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
        orderPanel.add(boxStatus, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

        //---- label6 ----
        orderPanel.add(employee, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
        orderPanel.add(boxEmployee, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

        //---- label4 ----
        orderPanel.add(amountTitle, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

        //---- label5 ----
        orderPanel.add(amount, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));

        // custom instellingen
        orderPanel.setPreferredSize(new Dimension(wrap.getWidth(), 30));
        orderPanelWrapper.setPreferredSize(new Dimension(wrap.getWidth(), 30));

        orderPanelWrapper.add(orderPanel,gbc,0);
        final HapManager member = this.hapManager;
        class MyItemStatusListener implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                boolean check = true;

                if(boxStatus.getSelectedIndex() <= getIndexItemStatusForCombobox(item.getStatusId())-1){check = false;}
                if(boxStatus.getSelectedIndex() == 0){check = false;}


                if (check == true){
                    if ("comboBoxChanged".equals(e.getActionCommand())) {
                        try{
                            Object status = boxStatus.getSelectedItem();
                            String statusString = status.toString();
                            int statusId = getItemStatusIdFromCombobox(statusString);
                            item.setStatusId(statusId);
                            member.changeItemStatus(order,item);
                        }catch(ChangeItemStatusException cise){
                            JOptionPane.showMessageDialog(new JFrame(),
                                    cise.toString());
                        }catch(HeadChefChangeStatusException hccse){
                            JOptionPane.showMessageDialog(new JFrame(),
                                    hccse.toString());
                        }
                    }
                    drawOrder(order);
                }else{
                    boxStatus.setSelectedIndex(getIndexItemStatusForCombobox(item.getStatusId()));
                }

            }
        }

        class ShowItem implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                try{
                    int item_id = Integer.parseInt(hiddenField.getText());
                    member.changeItem(order.getOrderId(),item_id);
                    member.setDishView();
                }catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(new JFrame(),
                            nfe.toString());
                }

            }
        }
        boxStatus.addActionListener(new MyItemStatusListener());
        itemButton.addActionListener(new ShowItem());

        return orderPanelWrapper;
    }

    /**
     *
     * initilisation of components
     */
    private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY
		// //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        panel4 = new JPanel();
        button3 = new JButton();
        panel2 = new JPanel();
        label1 = new JLabel();
        label2 = new JLabel();
        label7 = new JLabel();
        comboBox3 = new JComboBox();
        button2 = new JButton();
        panel1 = new JPanel();
        scrollPane1 = new JScrollPane();
        panel3 = new JPanel();
        button1 = new JButton();
        label3 = new JLabel();
        comboBox4 = new JComboBox();
        label6 = new JLabel();
        comboBox2 = new JComboBox();
        label4 = new JLabel();
        label5 = new JLabel();

        //======== this ========
        setLayout(new GridBagLayout());
        ((GridBagLayout)getLayout()).columnWeights = new double[] {1.0};
        ((GridBagLayout)getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0};

        //======== panel4 ========
        {
            panel4.setLayout(new GridBagLayout());
            ((GridBagLayout)panel4.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)panel4.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel4.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)panel4.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- button3 ----
            button3.setText("Terug");
            panel4.add(button3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
        }
        add(panel4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));

        //======== panel2 ========
        {
            panel2.setLayout(new GridBagLayout());
            ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
            ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- label1 ----
            label1.setText("Ordernummer:");
            panel2.add(label1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

            //---- label2 ----
            label2.setText("1");
            panel2.add(label2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

            //---- label7 ----
            label7.setText("status:");
            panel2.add(label7, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            panel2.add(comboBox3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

            //---- button2 ----
            button2.setText("Order plannen");
            panel2.add(button2, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
        }
        add(panel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));

        //======== panel1 ========
        {
            panel1.setLayout(new GridBagLayout());
            ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {0, 0};
            ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
            ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

            //======== scrollPane1 ========
            {

                //======== panel3 ========
                {
                    panel3.setLayout(new GridBagLayout());
                    ((GridBagLayout)panel3.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                    ((GridBagLayout)panel3.getLayout()).rowHeights = new int[] {0, 0};
                    ((GridBagLayout)panel3.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
                    ((GridBagLayout)panel3.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

                    //---- button1 ----
                    button1.setText("Bekijk gerecht");
                    panel3.add(button1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.BASELINE, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 0, 5), 0, 0));

                    //---- label3 ----
                    label3.setText("status:");
                    panel3.add(label3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.BASELINE, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 0, 5), 0, 0));
                    panel3.add(comboBox4, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.BASELINE, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 0, 5), 0, 0));

                    //---- label6 ----
                    label6.setText("Medewerker:");
                    panel3.add(label6, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.BASELINE, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 0, 5), 0, 0));
                    panel3.add(comboBox2, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.BASELINE, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 0, 5), 0, 0));

                    //---- label4 ----
                    label4.setText("Aantal");
                    panel3.add(label4, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.BASELINE, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 0, 5), 0, 0));

                    //---- label5 ----
                    label5.setText("2");
                    panel3.add(label5, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.BASELINE, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 0, 0), 0, 0));
                }
                scrollPane1.setViewportView(panel3);
            }
            panel1.add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        }
        add(panel1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
		// //GEN-END:initComponents
		modifyComponents();
	}

    /**
     * modify components after initilisation
     */
	public void modifyComponents() {
		scrollPane1.remove(panel3);
        comboBox3.setModel(statusModel);

        class MyStatusListener implements ActionListener {

            public void actionPerformed(ActionEvent evt) {


                boolean check = true;
                if(comboBox3.getSelectedIndex() == 3){check = false;}
                if(comboBox3.getSelectedIndex() == 4){check = false;}
                if(comboBox3.getSelectedIndex() > getIndexStatusForCombobox(order.getStatusId())+1){check = false;}
                if(comboBox3.getSelectedIndex() <= getIndexStatusForCombobox(order.getStatusId())){check = false;}

                if(statusFlag == 0){check = false;}

                if (check == true){
                
                    if ("comboBoxChanged".equals(evt.getActionCommand())) {
                        try{
                            Object status = comboBox3.getSelectedItem();
                            String statusString = status.toString();
                            int statusId = getStatusIdFromCombobox(statusString);

                            String input = JOptionPane.showInputDialog(null, "Chefkok geef u Code:", "Authorisatie Chefkok",
                                    JOptionPane.WARNING_MESSAGE);

                            int code = Integer.parseInt(input);

                            User user = employeeHmList.get(code);
                            if(user != null && user instanceof HeadChef ){
                                order.setStatusId(statusId);
                                if(statusId == 3){
                                    hapManager.removeOrderFromHm(order.getOrderId());
                                }
                                OrdersGui secondGui = hapManager.getOrdersView();
                                hapManager.changeOrderStatus(order);
                                secondGui.doFindOrders();
                            }else{
                                throw new ChefException(user);
                            }

                        }catch(NoActiveOrdersException naoe){
                            JOptionPane.showMessageDialog(new JFrame(),
                                    naoe.toString());
                        }catch(ChangeStatusException cse){
                            JOptionPane.showMessageDialog(new JFrame(),
                                    cse.toString());
                        }catch(ChefException cfe){
                            JOptionPane.showMessageDialog(new JFrame(),
                                    cfe.toString());
                        }
                    }
                    setOrderStatusComboBox();
                }else{
                    comboBox3.setSelectedIndex(getIndexStatusForCombobox(order.getStatusId()));
                }
            }
        }

        MyStatusListener actionListener = new MyStatusListener();
        comboBox3.addActionListener(actionListener);
	}
	// JFormDesigner - Variables declaration - DO NOT MODIFY
	// //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JPanel panel4;
    private JButton button3;
    private JPanel panel2;
    private JLabel label1;
    private JLabel label2;
    private JLabel label7;
    private JComboBox comboBox3;
    private JButton button2;
    private JPanel panel1;
    private JScrollPane scrollPane1;
    private JPanel panel3;
    private JButton button1;
    private JLabel label3;
    private JComboBox comboBox4;
    private JLabel label6;
    private JComboBox comboBox2;
    private JLabel label4;
    private JLabel label5;
	// JFormDesigner - End of variables declaration //GEN-END:variables
	private JPanel wrap;
    private ArrayList<Item> list;
}
