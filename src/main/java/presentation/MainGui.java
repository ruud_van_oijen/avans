package presentation;

import businesslogic.HapManager;

import java.awt.Window;
import java.lang.reflect.Method;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class MainGui {

    private JFrame mainGUI;
    private HapManager hapManager;
    private JPanel container;
    private CardLayout cl;
    private static String OS = System.getProperty("os.name").toLowerCase();

    public MainGui(HapManager manager) {
        if (isMacOSX()) {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Full Screen");
        }
        hapManager = manager;
        mainGUI = new JFrame();
        container = new JPanel();
        cl = new CardLayout();
        container.setLayout(cl);

        hapManager.setCl(cl);
        hapManager.setContainer(container);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                //do your stuff
                hapManager.removeActiveEmployees();
            }
        });


        initComponents();
        LoginGui loginGui = new LoginGui(hapManager);
        OrdersGui ordersGui = new OrdersGui(hapManager);

        hapManager.setSecondGui(ordersGui);

        container.add(loginGui, "login");
        container.add(ordersGui, "second");
        container.setSize(1024, 768);

        cl.show(container, "login");

        mainGUI.add(container);
        mainGUI.setContentPane(container);
        mainGUI.setTitle("De Hartige Hap");
        mainGUI.setSize(1024, 768);

        if (isMacOSX()) {
            enableFullScreenMode(mainGUI);
            mainGUI.setExtendedState(JFrame.MAXIMIZED_BOTH);
        } else if (isWindows()) {
            mainGUI.setExtendedState(JFrame.MAXIMIZED_BOTH);
        } else {
            mainGUI.setExtendedState(JFrame.MAXIMIZED_BOTH);
        }
        mainGUI.setVisible(true);
    }

    /**
     * initialisation of the components JFrame
     */
    private void initComponents() {

        mainGUI.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(mainGUI.getContentPane());
        mainGUI.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 300, Short.MAX_VALUE)
        );

        mainGUI.pack();
    }

    /**
     * enable full screen mode for a mac application
     *
     * @param window of the application
     */
    public static void enableFullScreenMode(Window window) {
        String className = "com.apple.eawt.FullScreenUtilities";
        String methodName = "setWindowCanFullScreen";

        try {
            Class<?> clazz = Class.forName(className);
            Method method = clazz.getMethod(methodName, new Class<?>[]{
                    Window.class, boolean.class});
            method.invoke(null, window, true);
        } catch (Exception t) {
            System.out.print("Full screen mode is not supported" + t);
        }
    }

    /**
     * @return true if program is running on mac
     */
    private static boolean isMacOSX() {
        return (OS.indexOf("mac") >= 0);
    }

    /**
     * @return true if program is running on windows
     */

    public static boolean isWindows() {
        return (OS.indexOf("win") >= 0);
    }

    /**
     * @return true if program is running on unix
     */
    public static boolean isUnix() {
        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
    }
}
