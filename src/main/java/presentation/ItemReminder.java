package presentation;


import domain.Employee;
import domain.Item;
import domain.User;

import javax.swing.*;
import java.awt.Toolkit;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ruudoijen on 08-07-15.
 *
 * @author  ruudoijen
 */
public class ItemReminder {
    private Toolkit toolkit;
    private JOptionPane jOptionPane;
    private java.util.Timer timer;

    public ItemReminder(Item item, String naam) {
        toolkit = Toolkit.getDefaultToolkit();
        timer = new java.util.Timer();
        jOptionPane = new JOptionPane();

        //timer.schedule(new RemindTask("U moet aan " + item.getItemName() +" beginnen "+ naam +"!"), (item.getRealpreperationTime()*60) * 1000);
        timer.schedule(new RemindTask("U moet aan " + item.getItemName() +" beginnen "+ naam +"!"), 5 * 1000);
    }

    class RemindTask extends TimerTask {

        private String message;
        private String title;

        public RemindTask(String message) {
            this.message = message;
            this.title = new String("Melding Gerecht");
        }

        public void run() {
            jOptionPane.showMessageDialog(null, " "+message+" ", ""+title+"", jOptionPane.INFORMATION_MESSAGE);
            toolkit.beep();
            timer.cancel(); //Not necessary because we call System.exit
            //System.exit(0); //Stops the AWT thread (and everything else)
        }
    }
}