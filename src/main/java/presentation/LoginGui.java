package presentation;

import javax.swing.*;
import java.awt.*;
import businesslogic.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class LoginGui extends JPanel {

	private HapManager hapManager;
	private JButton button01;
	private JLabel loginLabel, companyTitle, logo;
	private JTextField inputField01;

	public LoginGui(HapManager hapManager) {

		this.hapManager = hapManager;

		inputField01 = new JTextField();
		loginLabel = new JLabel("Medewerkernummer");
		companyTitle = new JLabel("De Hartige Hap");
		button01 = new JButton("Inloggen");
        ImageIcon icon = createImageIcon("logo_resized.jpg", "De Hartige Hap");

		logo = new JLabel(icon);

		companyTitle.setHorizontalAlignment(JLabel.CENTER);
		loginLabel.setFont(Font.getFont("Verdana"));

		loginLabel.setHorizontalAlignment(JLabel.RIGHT);
		loginLabel.setFont(Font.getFont("Verdana"));

		inputField01.setColumns(14);
		inputField01.setHorizontalAlignment(JTextField.CENTER);
		inputField01.setFont(Font.getFont("Verdana"));

		button01.setHorizontalAlignment(JButton.LEFT);

		JPanel grid01 = new JPanel();
		grid01.setLayout(new FlowLayout());

		this.setSize(800, 350);
		this.setLayout(new GridLayout(3, 1));
		this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

        this.add(logo);
		this.add(companyTitle);
		this.add(grid01);
		grid01.add(loginLabel);
		grid01.add(inputField01);
		grid01.add(button01);
		final HapManager member = hapManager;

        class EmployeeLogin implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                String inputString = inputField01.getText();
                ///try and catch
                try{
                    int employeeNumber = Integer.parseInt(inputString);
                    member.findChefkok(employeeNumber);
                    member.showSecondGui();
                    inputField01.setText("");

                }catch(ChefException use){
                    JOptionPane.showMessageDialog(new JFrame(),
                            use.toString());
                    inputField01.setText("");
                    inputField01.requestFocus();
                }
                catch(InvalidEmployeeException iue){
                    JOptionPane.showMessageDialog(new JFrame(),
                            iue.toString());
                    inputField01.setText("");
                    inputField01.requestFocus();

                }catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Dit is geen medewerkernummer");
                    inputField01.setText("");
                    inputField01.requestFocus();
                }catch(NullPointerException npe){
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Dit is geen medewerkernummer");
                    inputField01.setText("");
                    inputField01.requestFocus();
                }

            }
        }

        button01.addActionListener(new EmployeeLogin());
        inputField01.addActionListener(new EmployeeLogin());
	}

    /**
     * Returns an ImageIcon, or null if the path was invalid.
     *
     * @param path of the icon
     * @param description description of the image
     * @return imageicon
     */
    protected ImageIcon createImageIcon(String path, String description) {

        //Get file from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        java.net.URL imgURL = classLoader.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

}
