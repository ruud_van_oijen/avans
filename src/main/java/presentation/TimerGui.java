package presentation;

import businesslogic.HapManager;
import domain.Item;
import domain.Order;
import domain.User;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ruudoijen on 08-07-15.
 *
 * @author ruudoijen
 */
public class TimerGui {

    private ArrayList<Item> items;
    private HashMap<Integer, User> hmUsers;
    private HapManager hapManager;


    public TimerGui(HapManager manager, Order order){
        this.items = order.getItemArrayList();
        this.hapManager = manager;
        this.hmUsers = hapManager.getHmIdEmployees();

        for(Item item : items){
            int userId = item.getEmployeeId();
            User user = hmUsers.get(userId);
            String username = user.getEmployeeFirstName()+" "+user.getEmployeeLastName();
            new ItemReminder(item, username);
        }
    }


}
