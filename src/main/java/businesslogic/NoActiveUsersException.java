package businesslogic;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class NoActiveUsersException extends Exception {
    /**
     * Constructor
     */
    public NoActiveUsersException()
    {

    }

    /**
     * Override for the string representation of the exception.
     *
     * @return string there is nobody logged on.
     */
    @Override
    public String toString()
    {
        return "Er is niemand ingelogd";
    }
}
