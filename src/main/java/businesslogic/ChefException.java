package businesslogic;

import domain.User;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class ChefException extends Exception {

    private User user;

    /**
     * Constructor
     *
     * @param user the user that isn't a head chef
     */
    public ChefException(User user)
    {
        this.user = user;
    }

    /**
     * Override for the string representation of the exception.
     *
     * @return string U are not a head chef including employee's fullname
     */
    @Override
    public String toString()
    {
        return "U bent geen Chefkok " + this.user.getEmployeeFirstName() + " " + this.user.getEmployeeLastName();
    }
}