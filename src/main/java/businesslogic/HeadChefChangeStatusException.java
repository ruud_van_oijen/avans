package businesslogic;


import domain.Order;

/**
 * Created by ruudoijen on 08-07-15.
 *
 * @author ruudoijen
 */
public class HeadChefChangeStatusException extends Exception {
    private Order order;

    public HeadChefChangeStatusException(Order order){
        this.order = order;
    }
    /**
     * Override for the string representation of the exception.
     *
     * @return string
     */
    @Override
    public String toString()
    {
        return "Chefkok u kunt de status wijzigen van order:" +order.getOrderId();
    }
}
