package businesslogic;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class InvalidEmployeeException extends Exception {

    public InvalidEmployeeException(){};

    /**
     * Override for the string representation of the exception.
     *
     * @return string U are not a user of this system
     */
    @Override
    public String toString()
    {
        return "U bent geen gebruiker van dit systeem ";
    }

}
