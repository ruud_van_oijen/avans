package businesslogic;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class NoActiveOrdersException extends Exception {

    /**
     * Override for the string representation of the exception.
     *
     * @return string there are no current orders.
     */
    @Override
    public String toString()
    {
        return "Geen actieve orders";
    }
}
