package businesslogic;

import domain.User;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class HeadChefException extends Exception {

    private User user;

    /**
     * Constructor
     *
     * @param user The user that is the new head chef.
     */
    public HeadChefException(User user){
        this.user = user;
    }
    /**
     * Override for the string representation of the exception.
     *
     * @return string who is the new head chef.
     */
    @Override
    public String toString()
    {
        return "U bent de nieuwe Chefkok " + this.user.getEmployeeFirstName() + " " + this.user.getEmployeeLastName() + "";
    }
}
