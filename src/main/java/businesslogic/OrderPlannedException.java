package businesslogic;

/**
 * Created by ruudoijen on 06-07-15.
 *
 * @author ruudoijen
 */
public class OrderPlannedException extends Exception {

    /**
     * Override for the string representation of the exception.
     *
     * @return string the order is already planned
     */
    @Override
    public String toString()
    {
        return "Er is al een order ingepland";
    }
}
