package businesslogic;

import domain.Status;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class ChangeStatusException extends Exception {

    private Status status;

    public ChangeStatusException(Status status){
        this.status = status;
    }
    /**
     * Override for the string representation of the exception.
     *
     * @return string Status is being changed to current status
     */
    @Override
    public String toString()
    {
        return "Status is gewijzigd naar "+status.getStatusName();
    }
}
