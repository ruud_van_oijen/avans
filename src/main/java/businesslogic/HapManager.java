/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslogic;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import datastorage.*;
import domain.*;
import presentation.*;
import javax.swing.*;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class HapManager {

    private CardLayout cl, orderCl;
    private JPanel container, orderContainer;
    // hashmap for active employees by id
    private HashMap<Integer, User> hmIdEmployees;
    // hashmap for active employees by barcode
    private HashMap<Integer, User> hmEmployees;
    private HashMap<Integer, Order> hmOrders;
    private User chefkok;

    private OrderGui orderView;
    private ProfileGui profileView;
    private DishGui dishView;
    private OrdersGui ordersView;

//    private int howManyChefkoks;

    private HashMap<Integer, ItemStatus> itemStatusList;
    private HashMap<Integer, Status> statusList;
    private HashMap<Integer, Position> positionList;
    private HashMap<Integer, Category> categoryList;

    //test
    private HashMap<Integer, User> testhmEmployees;

    /**
     * constructor of the hapmanager
     */
    public HapManager() {
        StatusDAO statusDAO = new StatusDAO();
        statusList = statusDAO.getStatussen();

        ItemStatusDAO itemStatusDAO = new ItemStatusDAO();
        itemStatusList = itemStatusDAO.getStatussen();

        PositionDAO positionDAO = new PositionDAO();
        positionList = positionDAO.getPositions();

        CategoryDAO categoryDAO = new CategoryDAO();
        categoryList = categoryDAO.getCategories();


        this.hmIdEmployees = new HashMap();
        this.hmEmployees = new HashMap();
        this.hmOrders = new HashMap();
    }

    /**
     *
     * @return Ordergui panel
     */
    public OrdersGui getOrdersView() { return ordersView; }
    /**
     *
     * @return order status list
     */
    public HashMap<Integer, Status> getStatusList() { return statusList; }
    /**
     *
     * @return item categories list
     */
    public HashMap<Integer, Category> getCategoryList() { return categoryList; }
    /**
     *
     * @return employee positions list
     */
    public HashMap<Integer, Position> getPositionList() { return positionList; }

    /**
     *
     * @return item status list
     */
    public HashMap<Integer, ItemStatus> getItemStatusList() { return itemStatusList; }
    /**
     *
     * @return current employee list on barcode base
     */
    public HashMap<Integer, User> getHmEmployees() { return hmEmployees; }
    /**
     *
     * @return current employee list on user id base
     */
    public HashMap<Integer, User> getHmIdEmployees() { return hmIdEmployees; }

    /**
     *
     * @return current order list
     */
    public HashMap<Integer, Order> getHmOrders() { return hmOrders; }
    /**
     *
     * @return current active head chef
     */
    public User getChefkok() { return chefkok; }

    /**
     *
     * @param user set new head chef
     */
    public void setChefkok(User user){
        this.chefkok = user;
    }

    /**
     *
     * @param cl set cardlayout for order panel
     */
    public void setOrderCl(CardLayout cl) {
        this.orderCl = cl;
    }

    /**
     *
     * @param container container where the orderGui is wrapped
     */
    public void setOrderContainer(JPanel container) {
        this.orderContainer = container;
    }

    /**
     *
     * @param cl the cardlayout for the loginGui and OrdersGui
     */
    public void setCl(CardLayout cl) {
        this.cl = cl;
    }

    /**
     *
     * @param container container of the second cardlayout
     */
    public void setContainer(JPanel container) {
        this.container = container;
    }

    /**
     *
     * @param ordersView set ordersGui in the manager
     */
    public void setSecondGui(OrdersGui ordersView){ this.ordersView = ordersView; }
    /**
     *
     * @param orderView set the Orderview in the manager
     */
    public void setOrderView(OrderGui orderView){
        this.orderView = orderView;
    }
    /**
     *
     * @param profileView set the ProfileGui in the manager
     */
    public void setProfileView(ProfileGui profileView){
        this.profileView = profileView;
    }
    /**
     *
     * @param dishView set the DishGui in the manager
     */
    public void setDishView(DishGui dishView){
        this.dishView = dishView;
    }

    /**
     *
     * @param orderId change the order and draw it in Ordergui
     *
     * @throws InvalidOrderNumber this order number doesn't exist
     */
    public void changeOrder(int orderId) throws InvalidOrderNumber{
        if(hmOrders.containsKey(orderId)){
            Order order = hmOrders.get(orderId);
            orderView.drawOrder(order);
        }else{
            throw new InvalidOrderNumber();
        }
    }

    /**
     *
     * @param orderId ordernumber of the item that is in that order
     * @param itemId item id of the item I need. set the DishGUI to item I want
     */
    public void changeItem(int orderId ,int itemId){
        Order order = hmOrders.get(orderId);
        ArrayList<Item> items = order.getItemArrayList();
        Item item = getItemFromOrder(items, itemId);
        dishView.drawDish(item);
    }

    /**
     *
     * @param items arraylist of items in the order
     * @param itemId item I want out of the arraylist
     * @return specific item out of the arraylist
     */
    public Item getItemFromOrder(ArrayList<Item> items, int itemId){
        Item result = null;
        for(Item item : items){
            if(item.getItemId() == itemId){
                result = item;
            }
        }
        return result;
    }

    /**
     *
     * @param profileBarcode set profile of the user in the profileGUI
     */
    public void setProfile(int profileBarcode){
        User user = hmEmployees.get(profileBarcode);
        profileView.drawProfile(user);
    }

    /**
     *
     * @return true if the active hashmap is cleared and the active employees is emptied in the DAO
     */
    public boolean removeActiveEmployees(){
        // gooi hashmap leeg
        hmEmployees.clear();
        hmIdEmployees.clear();
        UserDAO userDAO = new UserDAO();
        boolean result = false;
        try{
            result = userDAO.removeActiveEmployees();
        }catch(NullPointerException npe){
            System.out.print(npe);
        }
        return result;
    }

    /**
     *
     * @param barcode of the employee
     * @throws InvalidEmployeeException the user is not a part of this system
     * @throws ChefException the user is a chef but not a head chef
     */
    public void findChefkok(int barcode) throws InvalidEmployeeException, ChefException {
        //Employee employee = testhmEmployees.get(barcode);

        User user;
        UserDAO userDAO = new UserDAO();
        user = userDAO.findEmployeeByBarcode(hmEmployees, barcode);

        if (user != null && user instanceof HeadChef) {
            setChefkok(user);
            hmEmployees.put(user.getEmployeeCode(), user);
            hmIdEmployees.put(user.getEmployeeId(), user);
            ordersView.doFindActiveEmployees();
        }else if(user != null && user instanceof Chef){
            throw new ChefException(user);
        }else{
            throw new InvalidEmployeeException();
        }

    }

    /**
     *
     * @param barcode ordersview screen second login for the employees
     * @throws InvalidEmployeeException if the employee is not a head chef or chef
     * @throws InvalidLogoutException head chef can not be logged out if other chefs are present
     * @throws NoActiveUsersException no users are logged in
     * @throws HeadChefException head chef is replaced by a new head chef
     */
    public void secondfindEmployee(int barcode) throws InvalidEmployeeException,InvalidLogoutException, NoActiveUsersException, HeadChefException {

        User user;
        UserDAO userDAO = new UserDAO();
        user = userDAO.findEmployeeByBarcode(hmEmployees, barcode);
        //15296 geen keuken medewerker
//        howManyChefkoks = 0;
//        System.out.print("Chefkok is " + chefkok.getEmployeeFirstName() + " " + chefkok.getEmployeeLastName() + "\n");
//        for(int key : hmEmployees.keySet()){
//            User user2 = hmEmployees.get(key);
//            if(user2 instanceof HeadChef){
//                howManyChefkoks++;
//                System.out.print(user2.getEmployeeFirstName()+ " "+ user2.getEmployeeLastName()+ "/"+howManyChefkoks+"//"+hmEmployees.size()+"/ \n");
//            }
//        }

        if(chefkok == null){
            showFirstGui();
            throw new NoActiveUsersException();
        }
        if (user.equals(chefkok) && user instanceof HeadChef) {
            if(hmEmployees.size() >= 2){
                //howManyChefkoks >= 1 &&
//                for (int key : hmEmployees.keySet()) {
//                    User user1 = hmEmployees.get(key);;
//                    if (user1 instanceof HeadChef && !user1.equals(chefkok) && howManyChefkoks >= 2 ) {
//                        hmEmployees.remove(user.getEmployeeCode());
//                        setChefkok(user1);
//                        ordersView.doFindActiveEmployees();
//                        throw new HeadChefException(user1);
//                    }else{
//                        System.out.print("1b \n");
//                        //throw new InvalidLogoutException(hmEmployees);
//                    } }

                throw new InvalidLogoutException(hmEmployees);

            }else{
                hmIdEmployees.remove(user.getEmployeeId());
                hmEmployees.remove(user.getEmployeeCode());
                setChefkok(null);
                ordersView.doFindActiveEmployees();
                showFirstGui();
            }

        } else if (user != null && user instanceof Chef || user instanceof HeadChef) {

            User user2 = hmEmployees.get(user.getEmployeeCode());
            if (user2 != null) {
                hmIdEmployees.remove(user.getEmployeeId());
                hmEmployees.remove(user.getEmployeeCode());
            } else {
                hmIdEmployees.put(user.getEmployeeId(), user);
                hmEmployees.put(user.getEmployeeCode(), user);
            }
            ordersView.doFindActiveEmployees();
        } else {
            throw new InvalidEmployeeException();
        }
    }

    /**
     *
     * @param order change the status of the order
     * @throws ChangeStatusException throw exception if the status has been changed
     */
    public void changeOrderStatus(Order order) throws ChangeStatusException{
        OrderDAO orderDAO = new OrderDAO();
        Order order1 = orderDAO.getOrder(order.getOrderId());
        if(order.getStatusId() == order1.getStatusId()){

        }else{
            orderDAO.updateOrderStatus(order);
            Status status = statusList.get(order.getStatusId());
            throw new ChangeStatusException(status);
        }
    }

    /**
     *
     * @param order the order of the item
     * @param item the item which status has been changed
     * @throws ChangeItemStatusException throw exception if the status has been changed
     * @throws HeadChefChangeStatusException throw exception if the chefkok must set order to ready
     */
    public void changeItemStatus(Order order, Item item) throws ChangeItemStatusException,HeadChefChangeStatusException{
        ItemDAO itemDAO = new ItemDAO();
        Item item1 = itemDAO.getItem(order.getOrderId(), item.getItemId());
        if(item.getStatusId() == item1.getStatusId()){

        }else{
            //itemDAO.updateItemStatus(order, item);
            User user = hmIdEmployees.get(item.getEmployeeId());
            user.removeItemfromUser(item1);
            ItemStatus status = itemStatusList.get(item.getStatusId());
            ArrayList<Item> items = order.getItemArrayList();
            ArrayList<Item> items2 = new ArrayList<>();

            for(Item item2 : items){
                items2.add(item2);
            }

            for(Item item3 : items2){
                if(item3.getStatusId() == 3){
                    items2.remove(item3);
                }
            }

            if(items2.size() == 0) {
                throw new HeadChefChangeStatusException(order);
            }else{
                throw new ChangeItemStatusException(status);
            }
        }
    }

    /**
     * get all orders from the DAO. if they don't exist already put them in the hashmap
     */
    public void findOrders() {
        //get all products
        ArrayList<Order>orderList = new ArrayList();
        OrderDAO orderDAO = new OrderDAO();
        orderList = orderDAO.findOrders();
        for(Order order : orderList){
            if(hmOrders.containsKey(order.getOrderId())){

            }else{
                hmOrders.put(order.getOrderId(),order);
            }
        }
    }

    /**
     * remove order for order hashmap
     * @param orderId order that needs to be removed from the active order list
     */
    public void removeOrderFromHm(int orderId){
        Order order = hmOrders.get(orderId);
        if(hmOrders.containsKey(orderId)){
            hmOrders.remove(order);
        }
    }

    /**
     * this function plans the order. only one order can be planned at a time.
     *
     * @param orderId the order that needs to be planned
     *
     * @throws PlannedOrderException throws this order is already planned
     * @throws OrderPlannedException throws if another order want's to be planned
     *
     */
    public void planOrder(int orderId) throws PlannedOrderException,OrderPlannedException{
        Order order = hmOrders.get(orderId);
        for(int key : hmOrders.keySet()){
            Order order2 = hmOrders.get(key);
            if(order2.getOrderPlan() && !order2.equals(order)){
                throw new OrderPlannedException();
            }
        }

        if(!order.getOrderPlan()) {
            order.setOrderPlan(true);

            /**
             * Categories
             * 1. Alcoholische // 2
             * 2. Dranken
             * 3. Voorgerechten
             * 4. Soepen // 3
             * 5. Salades // 3
             * 6. Vleesgerechten // 10
             * 7. Visgerechten // 10
             * 8. Pizza's // 10
             * 9. Pasta's // 10
             * 10. Hoofdgerechten
             * 11. Nagerechten
             * 12. Toetjes // 11
             */


            //welk category
            ArrayList<Item> items = order.getItemArrayList();

            ArrayList<Item> appetizer = new ArrayList<Item>();
            ArrayList<Item> mainCourse = new ArrayList<Item>();
            ArrayList<Item> desserts = new ArrayList<Item>();

            for (Item item : items) {
                if (item.getCategoryId() == 3 || item.getCategoryId() == 4 || item.getCategoryId() == 5) {
                    appetizer.add(item);
                } else if (item.getCategoryId() == 6 || item.getCategoryId() == 7 || item.getCategoryId() == 8 || item.getCategoryId() == 9 || item.getCategoryId() == 10) {
                    mainCourse.add(item);
                } else if (item.getCategoryId() == 11 || item.getCategoryId() == 12) {
                    desserts.add(item);
                } else {
                    System.out.print("No Category for kitchen");
                }
            }
            // custom comparator doesnt work
            // new ItemPreparationTimeComparator()
            // sort dishes on time
            Collections.sort(appetizer, Collections.reverseOrder());
            Collections.sort(mainCourse, Collections.reverseOrder());
            Collections.sort(desserts, Collections.reverseOrder());

            items.clear();
            items.addAll(appetizer);
            items.addAll(mainCourse);
            items.addAll(desserts);

            int divide = (int)Math.ceil(items.size() / hmIdEmployees.size());

            // voor elke item
            for(Item item : items) {
                for (int key : hmIdEmployees.keySet()) {
                    User user2 = hmIdEmployees.get(item.getEmployeeId());
                    // als item nog niet is gezet aan gebruiker
                    if(user2 == null){
                        // koppel item aan gebruiker
                        User user = hmIdEmployees.get(key);
                        int size = user.getItems().size();
                        //System.out.print(user.getEmployeeFirstName()+ " "+ size + " "+ divide + "\n");
                        // als de gebruiker kleiner is dan het berekende maximum
                        if (size < divide) {
                            user.addToItems(item);
                            item.setStatusId(2);
                            item.setEmployeeId(user.getEmployeeId());
                            item.setStatusId(2);
                        }else{
                            continue;
                        }
                    }else {
                        //System.out.print(user2.getEmployeeFirstName()+ " "+ item.getItemName() + " Deze gebruiker is gekoppeld \n");
                    }
                }
            }
            ordersView.redrawOrders(order);
            ItemDAO itemDAO = new ItemDAO();
            for(Item item : items){
                itemDAO.updateItemEmployee(order,item);
                //System.out.print(item.getEmployeeId() + "/" + item.getItemName() + "\n" );
            }


            order.setItemArrayList(items);
            orderView.drawOrder(order);
        }else{
            throw new PlannedOrderException();
        }

    }

    /**
     * show the orders overview
     */
    public void setOrdervieween() {
        orderCl.show(orderContainer, "ordersoverview");
    }

    /**
     * show the order overview of items
     */
    public void setOrderviewtwee() { orderCl.show(orderContainer, "itemsoverview"); }

    /**
     * show orders overview
     */
    public void showSecondGui(){  cl.show(container, "second"); }

    /**
     * show login screen
     */
    public void showFirstGui() { cl.show(container, "login"); }

    /**
     * show profile view
     */
    public void setProfileView() { orderCl.show(orderContainer, "profile"); }

    /**
     * show item overview
     */
    public void setDishView() { orderCl.show(orderContainer, "dish"); }
}
