package businesslogic;

import domain.ItemStatus;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class ChangeItemStatusException extends Exception {

    private ItemStatus status;

    public ChangeItemStatusException(ItemStatus status){
        this.status = status;
    }
    /**
     * Override for the string representation of the exception.
     *
     * @return string Status is being changed to current status
     */
    @Override
    public String toString()
    {
        return "Status is gewijzigd naar "+status.getStatusName();
    }
}
