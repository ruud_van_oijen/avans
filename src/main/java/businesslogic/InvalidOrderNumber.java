package businesslogic;

/**
 * Created by ruudoijen on 06-07-15.
 *
 * @author ruudoijen
 */
public class InvalidOrderNumber extends Exception {

    /**
     * Override for the string representation of the exception.
     *
     * @return string this order number doesn't exits anymore hit refresh
     */
    @Override
    public String toString()
    {
        return "Deze order bestaat niet meer. Druk op refresh";
    }
}
