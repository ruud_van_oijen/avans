package businesslogic;

import domain.User;

import java.util.HashMap;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class InvalidLogoutException extends Exception {

    private HashMap<Integer, User> hmEmployees;

    /**
     * Constructor
     *
     * @param hmEmployees all employees that are currently logged on.
     */
    public InvalidLogoutException(HashMap<Integer, User> hmEmployees)
    {
        this.hmEmployees = hmEmployees;
    }

    /**
     * Override for the string representation of the exception.
     *
     * @return string of every employee who is logged on. Chefkok has to be the last to log out
     */
    @Override
    public String toString()
    {
        String result = new String();
        for(int key : hmEmployees.keySet()){
            result += hmEmployees.get(key).getEmployeeFirstName() +"  "+ hmEmployees.get(key).getEmployeeLastName() + "\n";
        }
        return "Er zijn meerdere mensen ingelogd zoals \n" + result +" Chefkok moet als laatste Uitloggen";
    }
}
