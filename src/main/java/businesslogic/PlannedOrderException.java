package businesslogic;

/**
 * Created by ruudoijen on 06-07-15.
 *
 * @author ruudoijen
 */
public class PlannedOrderException extends Exception {

    /**
     * Override for the string representation of the exception.
     *
     * @return string the order is already planned
     */
    @Override
    public String toString()
    {
        return "Deze order is al ingepland";
    }
}
