package main;/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import businesslogic.HapManager;
import presentation.*;

/**
 * Created by ruudoijen on 23-06-15.
 *
 * @author ruudoijen
 */
public class Main {

	/**
	 *
	 * start of the application
	 *
	 * @param args
	 * the command line arguments
	 */
	public static void main(String[] args) {

		HapManager manager = new HapManager();
		MainGui gui = new MainGui(manager);
	}
}
