/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastorage;

import domain.Order;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class OrderDAO {

    private ArrayList<Order> orderList;

    public OrderDAO() {
    }

    /**
     *
     * @param orderId get the order from the database where the orderId matches order_id
     * @return the order
     */
    public Order getOrder(int orderId) {
        Order order = null;

        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            ResultSet resultset = connection.executeSQLSelectStatement("SELECT * FROM `view_orders_keuken` WHERE order_id = " + orderId + "");

            if (resultset != null) {
                try {

                    while (resultset.next()) {
                        int orderIdFromDb = resultset.getInt("order_id");
                        String sendOnFromDb = resultset.getString("send_on");
                        String lastUpdatedFromDb = resultset.getString("last_updated");
                        int statusFromDb = resultset.getInt("fk_status_id");
                        int employeeFromDb = resultset.getInt("fk_employee_id");

                        order = new Order(
                                orderIdFromDb,
                                sendOnFromDb,
                                lastUpdatedFromDb,
                                statusFromDb,
                                employeeFromDb
                        );
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    order = null;
                }
            }

            connection.closeConnection();
        }

        return order;
    }

    /**
     * get all the orders from the database
     *
     * @return arraylist of orders
     */
    public ArrayList<Order> findOrders() {
        Order order = null;

        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            ResultSet resultset = connection.executeSQLSelectStatement("SELECT * FROM `view_orders_keuken` order by last_updated ASC;");

            if (resultset != null) {
                try {
                    orderList = new ArrayList();
                    while (resultset.next()) {
                        int orderIdFromDb = resultset.getInt("order_id");
                        String sendOnFromDb = resultset.getString("send_on");
                        String lastUpdatedFromDb = resultset.getString("last_updated");
                        int statusFromDb = resultset.getInt("fk_status_id");
                        int employeeFromDb = resultset.getInt("fk_employee_id");

                        order = new Order(
                                orderIdFromDb,
                                sendOnFromDb,
                                lastUpdatedFromDb,
                                statusFromDb,
                                employeeFromDb
                        );

                        orderList.add(order);
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    order = null;
                }
            }

            connection.closeConnection();
        }

        return orderList;
    }

    /**
     * update the order status in the database
     *
     * @param order current order of the database
     */
    public void updateOrderStatus(Order order) {
        DatabaseConnection connection = new DatabaseConnection();

        if (connection.openConnection()) {
            connection.executeSQLUpdateStatement("UPDATE `order` SET `fk_status_id`='" + order.getStatusId() + "' WHERE `order_id`='" + order.getOrderId() + "';");
        }
        connection.closeConnection();

    }

    /**
     * get the orders where it matches the ordernumber
     *
     * @param orderNumber number of the order
     * @return arraylist of orders
     */
    public ArrayList<Order> findProduct(int orderNumber) {
        Order product = null;

        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            ResultSet resultset = connection.executeSQLSelectStatement("SELECT * FROM `order` WHERE order_id = " + orderNumber + ";");

            if (resultset != null) {

                try {

                    orderList = new ArrayList();
                    while (resultset.next()) {
                        int productIdFromDb = resultset.getInt("order_id");
                        String productNameFromDb = resultset.getString("send_on");
                        String productDescriptionFromDb = resultset.getString("last_updated");
                        int productStatusFromDb = resultset.getInt("fk_status_id");
                        int productEmployeeFromDb = resultset.getInt("fk_employee_id");

                        product = new Order(
                                productIdFromDb,
                                productNameFromDb,
                                productDescriptionFromDb,
                                productStatusFromDb,
                                productEmployeeFromDb
                        );

                        orderList.add(product);
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    product = null;
                }

            }
            connection.closeConnection();
        }

        return orderList;
    }


}
