package datastorage;


import domain.Item;
import domain.Order;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by jcoolen on 29/05/15.
 */
public class ItemDAO {

    private ArrayList<Item> itemList;

    public ItemDAO() {
    }

    /**
     *
     * @param orderId get the order from the database where the orderId matches order_id
     * @param itemid the id of the item in the order
     * @return the order
     */
    public Item getItem(int orderId, int itemid) {
        Item item = null;

        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            ResultSet resultset = connection.executeSQLSelectStatement("SELECT * FROM `view_orderline_keuken` WHERE fk_order_id = " + orderId + " AND fk_item_id = " + itemid + "  ");

            if (resultset != null) {
                try {

                    while (resultset.next()) {
                        int itemIdFromDb = resultset.getInt("fk_item_id");
                        String itemNameFromDb = resultset.getString("item_name");
                        int employeeIdFromDb = resultset.getInt("fk_employee_id");
                        String employeeNameFromDb = resultset.getString("employee_name");
                        String orderStatusFromDb = resultset.getString("order_status");
                        int totalcountintableFromDb = resultset.getInt("total_count_in_table");
                        int preparationTimeFromDb = resultset.getInt("preparationtime");
                        int categoryIdFromDb = resultset.getInt("category_id");
                        String itemDescriptionFromDb = resultset.getString("description");

                        item = new Item(itemIdFromDb, itemNameFromDb, employeeIdFromDb, employeeNameFromDb, orderStatusFromDb, totalcountintableFromDb, preparationTimeFromDb, categoryIdFromDb, itemDescriptionFromDb);

                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    item = null;
                }
            }

            connection.closeConnection();
        }

        return item;
    }

    /**
     * @param orderNumber number of the order
     * @return all items that are with the order
     */
    public ArrayList<Item> findOrderItems(int orderNumber) {
        Item item = null;

        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {

            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM `view_orderline_keuken` WHERE `fk_order_id` = " + orderNumber + ";");

            if (resultset != null) {

                try {
                    itemList = new ArrayList<Item>();
                    while (resultset.next()) {
                        int itemIdFromDb = resultset.getInt("fk_item_id");
                        String itemNameFromDb = resultset.getString("item_name");
                        int employeeIdFromDb = resultset.getInt("fk_employee_id");
                        String employeeNameFromDb = resultset.getString("employee_name");
                        String orderStatusFromDb = resultset.getString("order_status");
                        int totalcountintableFromDb = resultset.getInt("total_count_in_table");
                        int preparationTimeFromDb = resultset.getInt("preparationtime");
                        int categoryIdFromDb = resultset.getInt("category_id");
                        String itemDescriptionFromDb = resultset.getString("description");

                        item = new Item(itemIdFromDb, itemNameFromDb, employeeIdFromDb, employeeNameFromDb, orderStatusFromDb, totalcountintableFromDb, preparationTimeFromDb, categoryIdFromDb, itemDescriptionFromDb);

                        itemList.add(item);
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    item = null;
                }

            }
            connection.closeConnection();
        }

        return itemList;
    }

    /**
     * update the item employee in the database
     *
     * @param order order is the order that contains the item
     * @param item item that is being set to plan
     */
    public void updateItemEmployee(Order order, Item item) {
        DatabaseConnection connection = new DatabaseConnection();

        if (connection.openConnection()) {
            connection.executeSQLUpdateStatement("UPDATE `kpt_orderline` SET `fk_employee_id`='" + item.getEmployeeId() + "' WHERE  `fk_order_id` = '" + order.getOrderId() + "' AND fk_item_id = '" + item.getItemId() + "';");
        }
        connection.closeConnection();

    }

    /**
     * update the item status in the database
     *
     * @param order current order of the database
     * @param item itemstatus that is update in the db
     */
    public void updateItemStatus(Order order, Item item) {
        DatabaseConnection connection = new DatabaseConnection();

        if (connection.openConnection()) {
            connection.executeSQLUpdateStatement("UPDATE `kpt_orderline` SET `fk_itemstatus_id`='" + item.getStatusId() +"' WHERE  `fk_order_id` = '" + order.getOrderId() + "' AND fk_item_id = '" + item.getItemId() + "';");
        }
        connection.closeConnection();

    }

}
