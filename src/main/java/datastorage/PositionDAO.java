package datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import domain.Position;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class PositionDAO {

    private HashMap<Integer, Position> positions;

    public PositionDAO() {

        positions = new HashMap<Integer, Position>();

        fillPositions();

    }

    public HashMap<Integer, Position> getPositions() {
        return positions;
    }

    /**
     * get all the positions from the database and fill them in a hashmap
     */
    public void fillPositions() {
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM `" + connection.getDatabase() + "`.`position`;");

            if (resultset != null) {
                try {

                    while (resultset.next()) {
                        int positionIdFromDb = resultset.getInt("position_id");
                        String positionFromDb = resultset.getString("name");
                        Position pos = new Position(positionIdFromDb, positionFromDb);
                        positions.put(positionIdFromDb, pos);
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                }
            }
            connection.closeConnection();
        }
    }
}
