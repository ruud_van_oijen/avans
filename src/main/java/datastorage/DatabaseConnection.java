/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datastorage;

import java.sql.*;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class DatabaseConnection {

	private Connection connection;


	private String hostName;
	private int port;
    private String database;
	private String userName;
	private String passWord;

	private Statement statement;

    /**
     * constructor set the values of the database and user
     */
	public DatabaseConnection() {
		connection = null;
		statement = null;
		hostName = "mysql.famcoolen.nl";
		port = 3306;
		database= "avans_hartigehap_c3";
		userName = "ivp4c3";
		passWord = "Ixpebfy8mp";
//      database = "avans_hartigehap_oplevering";
//		userName = "ivp4_admin";
//		passWord = "hHzCPejUBA";
	}

    public String getDatabase() {
        return database;
    }

    /**
     * open a database connection
     * @return true if connection is open
     */
	public boolean openConnection() {
		boolean result = false;

		if (connection == null) {
			try {
				// Try to create a connection with the library database
				connection = DriverManager.getConnection(
						// "jdbc:mysql://mysql.famcoolen.nl/avans_hartigehap_c3"
						// , "ivp4c3", "Ixpebfy8mp");
						"jdbc:mysql://"+hostName+":"+port+"/"+database+"",
						""+userName+"", ""+passWord+"");

				if (connection != null) {
					statement = connection.createStatement();
				}

				result = true;
			} catch (SQLException e) {
				System.out.println(e);
				result = false;
			}
		} else {
			result = true;
		}

		return result;
	}

    /**
     * check if connection is open
     * @return true if connection is open
     */
	public boolean connectionIsOpen() {
		boolean open = false;

		if (connection != null && statement != null) {
			try {
				open = !connection.isClosed() && !statement.isClosed();
			} catch (SQLException e) {
				System.out.println(e);
				open = false;
			}
		}

		return open;
	}

    /**
     * close the database connection
     */
	public void closeConnection() {
		try {
			statement.close();
			connection.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

    /**
     *
     * @param query of the select statement
     * @return list of records
     */
	public ResultSet executeSQLSelectStatement(String query) {
		ResultSet resultset = null;

		if (query != null && connectionIsOpen()) {
			try {
				resultset = statement.executeQuery(query);
			} catch (SQLException e) {
				System.out.println(e);
				resultset = null;
			}
		}

		return resultset;
	}

    /**
     *
     * @param query delete statement
     * @return true if delete has succeeded
     */
	public boolean executeSQLDeleteStatement(String query) {
		boolean result = false;

		if (query != null && connectionIsOpen()) {
			try {
				statement.executeUpdate(query);
				result = true;
			} catch (SQLException e) {
				System.out.println(e);
				result = false;
			}
		}

		return result;
	}

    /**
     *
     * @param query of insert statement
     * @return true if insert has succeeded
     */
	public boolean executeSQLInsertStatement(String query) {
		boolean result = false;

		if (query != null && connectionIsOpen()) {
			try {
				statement.executeUpdate(query);
				result = true;
			} catch (SQLException e) {
				System.out.println(e);
				result = false;
			}
		}

		return result;
	}

    /**
     *
     * @param query of update statement
     * @return true if update has succeeded
     */
	public boolean executeSQLUpdateStatement(String query) {
		boolean result = false;

		if (query != null && connectionIsOpen()) {
			try {
				statement.executeUpdate(query);
				result = true;
			} catch (SQLException e) {
				System.out.println(e);
				result = false;
			}
		}

		return result;
	}
}
