package datastorage;

import domain.HeadChef;
import domain.Employee;
import domain.User;
import domain.Chef;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Johan on 13-5-2015.
 *
 * @version 2.0.0
 */
public class UserDAO {

    private ArrayList<User> userList;

    public UserDAO() {
        // Nothing to be initialized. This is a stateless class. Constructor
        // has been added to explicitely make this clear.
    }

    public boolean removeActiveEmployees(){

        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();

        boolean result = false;

        connection.openConnection();

        result = connection.executeSQLDeleteStatement("DELETE FROM "+connection.getDatabase()+".active_employee where `fk_employee_id` != null;");

        connection.closeConnection();

        return  result;
    }

    public User findEmployeeByBarcode(HashMap<Integer, User> hmEmployees, int EmployeeBarcode) {

        // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();

        // (1) get today's date
        Date today = Calendar.getInstance().getTime();

        // (2) create a date "formatter" (the date format we want)
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        // (3) create a new String using the date format we want
        String currentTime = formatter.format(today);

        User user;
        user = hmEmployees.get(EmployeeBarcode);


        if(user == null) {
            if (connection.openConnection()) {
                // If a connection was successfully setup, execute the SELECT statement.
                ResultSet resultset = connection.executeSQLSelectStatement(
                        "SELECT * FROM employee WHERE barcode = " + EmployeeBarcode + ";");

                if (resultset != null) {

                    try {
                        while (resultset.next()) {
                            int employeeIdFromDb = resultset.getInt("employee_id");
                            int employeeCodeFromDb = resultset.getInt("barcode");
                            String employeeFirstNameFromDb = resultset.getString("firstname");
                            String employeeLastNameFromDb = resultset.getString("lastname");
                            String employeeAddresFromDb = resultset.getString("address");
                            String employeeZipCodeFromDb = resultset.getString("zipcode");
                            String employeeCityFromDb = resultset.getString("city");
                            String employeeEmailFromDb = resultset.getString("email");
                            String employeeBirthDateFromDb = resultset.getString("date_of_birth");
                            int employeePositionFromDb = resultset.getInt("fk_position_id");

                            if(employeePositionFromDb == 1){
                                user = new HeadChef(
                                        employeeIdFromDb,
                                        employeeCodeFromDb,
                                        employeeFirstNameFromDb,
                                        employeeLastNameFromDb,
                                        employeeAddresFromDb,
                                        employeeZipCodeFromDb,
                                        employeeCityFromDb,
                                        employeeEmailFromDb,
                                        employeeBirthDateFromDb,
                                        employeePositionFromDb
                                );
                            }else if(employeePositionFromDb == 2){
                                user = new Chef(
                                        employeeIdFromDb,
                                        employeeCodeFromDb,
                                        employeeFirstNameFromDb,
                                        employeeLastNameFromDb,
                                        employeeAddresFromDb,
                                        employeeZipCodeFromDb,
                                        employeeCityFromDb,
                                        employeeEmailFromDb,
                                        employeeBirthDateFromDb,
                                        employeePositionFromDb
                                );
                            }else {
                                user = new Employee(
                                        employeeIdFromDb,
                                        employeeCodeFromDb,
                                        employeeFirstNameFromDb,
                                        employeeLastNameFromDb,
                                        employeeAddresFromDb,
                                        employeeZipCodeFromDb,
                                        employeeCityFromDb,
                                        employeeEmailFromDb,
                                        employeeBirthDateFromDb,
                                        employeePositionFromDb
                                );
                            }
                        }
                    } catch (SQLException e) {
                        System.out.println(e);
                        user = null;
                    }

                }
                connection.executeSQLInsertStatement("INSERT INTO `active_employee` (`fk_employee_id`, `starttime`, `endtime`) VALUES (\" " + user.getEmployeeId() + " \", \"" + currentTime + " \",\"" + currentTime + "\");");
                // We had a database connection opened. Since we're finished,
                // we need to close it.
                connection.closeConnection();
            }
        }else{
            if (connection.openConnection()) {
                //connection.executeSQLUpdateStatement("UPDATE `active_employee` SET  `endtime`='" + employee.getEmployeeId() + "' WHERE `fk_employee_id` =  (\"" + employee.getEmployeeId() + "\");");
                connection.executeSQLDeleteStatement("DELETE FROM `active_employee` WHERE `fk_employee_id` =  (\"" + user.getEmployeeId() + "\");");
            }
            connection.closeConnection();
        }

        return user;
    }

}
