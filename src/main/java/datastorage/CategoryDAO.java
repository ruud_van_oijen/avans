package datastorage;

import domain.Category;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class CategoryDAO {

    private HashMap<Integer, Category> categories;

    public CategoryDAO() {
        categories = new HashMap<Integer, Category>();

        fillCategories();
    }


    public HashMap<Integer, Category> getCategories() {
        return categories;
    }

    /**
     * get all categories from the database and put them in the hashmap
     */
    public void fillCategories() {
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM `" + connection.getDatabase() + "`.`category`;");

            if (resultset != null) {
                try {

                    while (resultset.next()) {
                        int categoryIdFromDb = resultset.getInt("category_id");
                        String categoryNameFromDb = resultset.getString("name");
                        Category category = new Category(categoryIdFromDb, categoryNameFromDb);

                        categories.put(category.getCategoryId(), category);
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                }
            }
            connection.closeConnection();
        }
    }
}

