package datastorage;

import domain.ItemStatus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class ItemStatusDAO {


    private HashMap<Integer, ItemStatus> statussen;

    public ItemStatusDAO() {
        statussen = new HashMap<Integer, ItemStatus>();

        fillStatussen();
    }


    public HashMap<Integer, ItemStatus> getStatussen() {
        return statussen;
    }

    /**
     * fill the hashmap with all the status in the database
     */
    public void fillStatussen() {

        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {

            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM `" + connection.getDatabase() + "`.`item_status`;");

            if (resultset != null) {
                try {

                    while (resultset.next()) {
                        int statusIdFromDb = resultset.getInt("item_status_id");
                        String statusFromDb = resultset.getString("name");
                        ItemStatus status = new ItemStatus(statusIdFromDb, statusFromDb);

                        statussen.put(status.getStatusId(), status);
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                }
            }
            connection.closeConnection();
        }
    }


}
