package datastorage;

import domain.Ingredient;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class IngredientDAO {

    private ArrayList<Ingredient> ingredientList;

    public IngredientDAO() {
    }

    /**
     * @param itemNumber itemnumber of the item
     * @return all ingredients of the item
     */
    public ArrayList<Ingredient> findItemIngredients(int itemNumber) {
        Ingredient ingredient = null;

        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {

            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM `view_itemingredient` WHERE `fk_item_id` = " + itemNumber + ";");

            if (resultset != null) {

                try {
                    ingredientList = new ArrayList<Ingredient>();
                    while (resultset.next()) {
                        int ingredientIdFromDb = resultset.getInt("fk_ingredient_id");
                        String itemNameFromDb = resultset.getString("ingredient_name");
                        int amountFromDb = resultset.getInt("amount");

                        ingredient = new Ingredient(ingredientIdFromDb, itemNameFromDb, amountFromDb);

                        ingredientList.add(ingredient);
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    ingredient = null;
                }

            }

            connection.closeConnection();
        }

        return ingredientList;
    }

}