package datastorage;

import domain.Status;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by ruudoijen on 29-06-15.
 *
 * @author ruudoijen
 */
public class StatusDAO {


    private HashMap<Integer, Status> statussen;

    public StatusDAO() {
        statussen = new HashMap<Integer, Status>();

        fillStatussen();
    }


    public HashMap<Integer, Status> getStatussen() {
        return statussen;
    }

    /**
     * get all the order status from the database and put them in the hashmap
     */
    public void fillStatussen() {
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM `" + connection.getDatabase() + "`.`status`;");

            if (resultset != null) {
                try {

                    while (resultset.next()) {
                        int statusIdFromDb = resultset.getInt("status_id");
                        String statusFromDb = resultset.getString("name");
                        Status status = new Status(statusIdFromDb, statusFromDb);

                        statussen.put(status.getStatusId(), status);
                    }
                } catch (SQLException e) {
                    System.out.println(e);
                }
            }
            connection.closeConnection();
        }
    }


}
